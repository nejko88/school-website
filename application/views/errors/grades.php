<br>
<div class="col-md-8 offset-md-2">
<div class="alert alert-dismissible alert-primary" style="border-radius:3px;" align="middle">
  <strong>No grades added.</strong>
</div>
</div>
<br>
<div style="text-align: center">
<a href="<?php echo base_url(); ?>grades" class="btn btn-outline-primary btn-large">Back</a>
</div>