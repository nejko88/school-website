<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('classes/add'); ?>
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group radio">
	<label>Year</label>
<select class="custom-select" name="year">
    <option value="1.">1.</option>
    <option value="2.">2.</option>
    <option value="3.">3.</option>
    <option value="4.">4.</option>
</select>
</div>
<div class="form-group radio">
	<label>Class</label>
<select class="custom-select" name="class">
    <option value="a">a</option>
    <option value="b">b</option>
    <option value="c">c</option>
    <option value="d">d</option>
    <option value="e">e</option>
	<option value="f">f</option>
	<option value="g">g</option>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>
</div>

<?php echo form_close(); ?>