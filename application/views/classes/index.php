<div class="col-md-6 offset-md-3">
<h1><?= $title; ?></h1><br>

<table class="table table-hover">
  <thead>
    <tr class="table-primary">
      <th scope="col">Name</th>
      <?php if ($this->session->userdata('role') == 'admin') : ?>
      <th scope="col"></th>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach($classes as $class) : ?>
    <tr class="table-light">
      <th scope="row">
      <a class="black" href="<?php echo base_url().'classes/'.$class['id']; ?>"><?php echo $class['class']; ?></a>
      </th>
      <?php if ($this->session->userdata('role') == 'admin') : ?>
      <td>
      <a class="btn-right" href="<?php echo site_url('classes/delete/'.$class['id'].'/'.$class['class']); ?>"><i class="material-icons">clear</i></a>
      <a class="btn-right" href="<?php echo site_url('classes/edit/'.$class['id']); ?>"><i class="material-icons">create</i></a>
      </td>
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>  
  </tbody>
</table> 
</div>