<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('classes/update/'.$class['id']); ?>
<input type="hidden" name="id" value="<?php echo $class['id'];?>">
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group radio">
	<label>Year</label>
<select class="custom-select" name="year">
    <option value="1." <?php echo ($year == 1.) ? 'selected' : ''; ?>>1.</option>
    <option value="2." <?php echo ($year == 2.) ? 'selected' : ''; ?>>2.</option>
    <option value="3." <?php echo ($year == 3.) ? 'selected' : ''; ?>>3.</option>
    <option value="4." <?php echo ($year == 4.) ? 'selected' : ''; ?>>4.</option>
</select>
</div>
<div class="form-group radio">
	<label>Class</label>
<select class="custom-select" name="class">
    <option value="a" <?php echo ($letter == 'a') ? 'selected' : ''; ?>>a</option>
    <option value="b" <?php echo ($letter == 'b') ? 'selected' : ''; ?>>b</option>
    <option value="c" <?php echo ($letter == 'c') ? 'selected' : ''; ?>>c</option>
    <option value="d" <?php echo ($letter == 'd') ? 'selected' : ''; ?>>d</option>
    <option value="e" <?php echo ($letter == 'e') ? 'selected' : ''; ?>>e</option>
	<option value="f" <?php echo ($letter == 'f') ? 'selected' : ''; ?>>f</option>
	<option value="g" <?php echo ($letter == 'g') ? 'selected' : ''; ?>>g</option>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>
</div>

<?php echo form_close(); ?>