<div class="col-md-8 offset-md-2">
<h1><?= $title; ?></h1><br>

<table class="table table-hover">
  <thead>
    <tr class="table-primary">
      <th scope="col">Name</th>
      <?php if ($this->session->userdata('role') == 'admin') : ?>
      <th scope="col"></th>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach($users as $user) : ?>
    <tr class="table-light">
      <th scope="row"><a class="black" href="<?php echo base_url().'users/'.$user['id']; ?>"><?php echo $user['name']; ?></a></th>
      <?php if ($this->session->userdata('role') == 'admin') : ?>
      <td>
      <a class="btn-right" href="<?php echo site_url('users/delete/'. $user['id'].'/'.$user['role']); ?>"><i class="material-icons">clear</i></a>
      <a class="btn-right" href="<?php echo site_url('users/edit_student/'.$user['id']); ?>"><i class="material-icons">create</i></a>
      </td>
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>  
  </tbody>
</table> 
</div>