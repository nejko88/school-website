<html>
<head>
<title>School</title>
<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/assets/img/logo.png" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/pulse/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<nav id="navbar" class="navbar navbar-expand-lg navbar-dark bg-primary">
    <form class="form-inline my-2 my-lg-0" id="search-responsive" action="<?php echo site_url('users/search');?>" method="post" accept-charset="utf-8">
      <input style="display: inline;" class="form-control mr-sm-2 input-search" name="keyword" type="text" placeholder="Search" required>
      <button style="display: inline; vertical-align:middle;" class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
    <button class="navbar-toggler" style="border:none;padding:0" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="true" aria-label="Toggle navigation" style="">
    <span class="navbar-toggler-icon"></span>
    </button>
  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav" style="width:45%;">
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>">Home</a></li>
      
      <?php if ($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'teacher') : ?>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>classes">Classes</a></li> 
      <?php endif; ?>

      <?php if ($this->session->userdata('role') == 'admin') : ?>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>users/teachers">Teachers</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>subjects">Subjects</a></li>
      <?php endif; ?>

      <?php if ($this->session->userdata('role') == 'teacher') : ?>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>grades">Grades</a></li>
      <?php endif; ?>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Schedules</a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="<?php echo base_url(); ?>schedules/classes">Classes</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>schedules/teachers">Teachers</a>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0" id="search" action="<?php echo site_url('users/search');?>" method="post" accept-charset="utf-8">
      <input class="form-control mr-sm-2 input-search" name="keyword" type="text"placeholder="Search" required>
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
    <ul class="navbar-nav ml-auto">
      <?php if ($this->session->userdata('role') != '') : ?>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Add</a>
        <div class="dropdown-menu">
          <?php if ($this->session->userdata('role') == 'student') : ?>
          <a class="dropdown-item" href="<?php echo base_url(); ?>exams/add_m">Exam</a>
          <?php endif; ?>
          <?php if ($this->session->userdata('role') == 'teacher') : ?>
          <a class="dropdown-item" href="<?php echo base_url(); ?>exams/add_c">Exam</a>
          <?php endif; ?>
          <?php if ($this->session->userdata('role') == 'admin') : ?>
          <a class="dropdown-item" href="<?php echo base_url(); ?>users/add_teacher">Teacher</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>users/add_student">Student</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>classes/add">Class</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>subjects/add">Subject</a>
          <a class="dropdown-item" href="<?php echo base_url(); ?>schedules/add">Schedule</a>
          <?php endif; ?>
        </div>
      </li>
      <?php endif; ?>
      
      <?php if ($this->session->userdata('role') == 'student' || $this->session->userdata('role') == 'teacher') : ?>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>profile">Profile</a></li>
      <?php endif; ?>

      <?php if ($this->session->userdata('role') != '') : ?>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>users/logout">Logout</a></li>
      <?php endif; ?>

      <?php if ($this->session->userdata('role') == '') : ?>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>login">Login</a></li>
      <?php endif; ?>

    </ul>
  </div>
</nav>

<div class="container" id="container" style="margin-top: 50px">

<?php if ($this->session->flashdata('user_registered')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('login_failed')): ?>
	<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('user_loggedin')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('user_loggedout')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('user_updated')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_updated').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('user_deleted')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_deleted').'</p>'; ?>
<?php endif; ?>



<?php if ($this->session->flashdata('schedule_added')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('schedule_added').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('schedule_deleted')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('schedule_deleted').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('schedule_updated')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('schedule_updated').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('schedule_exists')): ?>
	<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('schedule_exists').'</p>'; ?>
<?php endif; ?>



<?php if ($this->session->flashdata('class_added')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('class_added').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('class_deleted')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('class_deleted').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('class_updated')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('class_updated').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('class_exists')): ?>
	<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('class_exists').'</p>'; ?>
<?php endif; ?>



<?php if ($this->session->flashdata('subject_added')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('subject_added').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('subject_deleted')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('subject_deleted').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('subject_updated')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('subject_updated').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('subject_exists')): ?>
	<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('subject_exists').'</p>'; ?>
<?php endif; ?>



<?php if ($this->session->flashdata('exam_added')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('exam_added').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('exam_exists')): ?>
	<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('exam_exists').'</p>'; ?>
<?php endif; ?>

<?php if ($this->session->flashdata('exam_deleted')): ?>
	<?php echo '<p class="alert alert-success">'.$this->session->flashdata('exam_deleted').'</p>'; ?>
<?php endif; ?>
