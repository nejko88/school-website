<div class="col-md-6 offset-md-3">
<h1><?= $title; ?></h1><br>

<table class="table table-hover" id="subjects">
  <thead>
    <tr class="table-primary">
      <th scope="col">Subject</th>
      <th scope="col">Teacher</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($subjects as $subject) : ?>
    <tr class="table-light">
      <th scope="row"><?php echo ucfirst($subject['subject']); ?></th>
      <td><?php echo $subject['teacher']; ?></td>
      <td>
      <a class="btn-right" href="<?php echo site_url('subjects/delete/'.$subject['id'].'/'.str_replace(' ', '-', $subject['teacher']).'_'.$subject['subject']); ?>">
      <i class="material-icons">clear</i>
      </a>
      </td>
    </tr>
    <?php endforeach; ?>  
  </tbody>
</table> 
</div>
