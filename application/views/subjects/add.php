<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('subjects/add'); ?>
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group">
	<label>Subject</label>
	<input type="text" class="form-control" name="subject" placeholder="Subject" maxlength="28">
</div>
<div class="form-group radio">
	<label>Teacher</label>
<select class="custom-select" name="teacher">
    <?php foreach($teachers as $teacher) : ?>
    <option value="<?php echo $teacher; ?>"><?php echo $teacher; ?></option>
    <?php endforeach; ?>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>
</div>

<?php echo form_close(); ?>