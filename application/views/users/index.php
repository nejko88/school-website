<div class="col-md-8 offset-md-2">
<h1><?= ucfirst($title).'s'; ?></h1><br>

<table class="table table-hover">
  <thead>
    <tr class="table-primary">
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($users as $user) : ?>
    <tr class="table-light table-teachers">
      <th scope="row"><a class="black" href="<?php echo base_url().'users/'.$user['id']; ?>"><?php echo $user['name']; ?></a></th>
      <td><?php echo $user['email']; ?></td>
      <td>
      <a class="btn-right" href="<?php echo site_url('users/delete/'. $user['id'].'/'.$user['role']); ?>"><i class="material-icons">clear</i></a>
      <a class="btn-right" href="<?php echo site_url('users/edit_'.$title.'/'.$user['id']); ?>"><i class="material-icons">create</i></a>
      </td>
    </tr>
    <?php endforeach; ?>  
  </tbody>
</table> 
</div>