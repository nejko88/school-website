<?php if($user['role'] == 'student') : ?>

<div class="col-md-8" style="padding:0;">
<div class="card border-primary mb-3">
  <div class="card-header"><b><?php echo $user['name'].', '.$user['class']; ?></b><a class="btn btn-primary btn-right" href="<?php echo base_url().'schedules/classes/'.$class_id; ?>">Schedule</a></div>
  <div class="card-body">
    <p><b>Address: &nbsp;</b><?php echo $user['address']; ?></p>
    <p><b>Email: &nbsp;</b><?php echo $user['email']; ?></p></p>
  </div>
</div>
</div>
<br>

<h3>Grades</h3><br>
	<table class="table table-bordered table-grades">
		<thead>
			<tr class="table-active">
				<th>Subject</th>
				<th>1. semester</th>
				<th>2. semester</th>
        <th>3. semester</hd>
        <th>Average</th>
			</tr>
		</thead>
		<tbody>
    <?php foreach($subjects as $subject) : ?>
    <tr class="table-light">
      <th scope="row"><?php echo ucfirst($subject); ?></th>
      <?php $j = 0; $k = 0; ?>
      <?php for ($i=1; $i < 4; $i++) : ?>
      <td class="grade">
      <?php $l = 0; ?>
      <?php foreach($grades as $grade) : ?>
      <?php if ($grade['subject'] == $subject && $grade['semester'] == $i) { 
        echo $grade['grade'].'&nbsp;&nbsp;';
        $k += $grade['grade'];
        $j += 1;
        $l += 1;
      } ?>
      <?php endforeach; ?>
      <?php echo ($l == 0 ? '/' : ''); ?>
      </td>
      <?php endfor; ?>
      <td><?php echo ($j != 0 ? round($k/$j, 1) : '/'); ?></td>
    </tr>
    <?php endforeach; ?>
		</tbody>
	</table>
<br>
<div id="average" style="font-size:18px;" class="alert alert-dismissible alert-primary">
  Average grade: &nbsp;<strong><?= $avg; ?></strong>.
</div>
<script>
  document.getElementById("navbar").style.minWidth="749px";
  document.getElementsByClassName("container")[0].style.minWidth="729px";
</script>
<?php endif; ?>

<?php if($user['role'] == 'teacher') : ?>
<div class="col-md-8 offset-md-2">
<h2><?php echo $user['name']; ?></h2><br>
<div class="card border-primary mb-3">
  <div class="card-header"><b>Info</b><a class="btn btn-primary btn-right" href="<?php echo base_url().'schedules/teachers/'.$user['id']; ?>">Schedule</a></div>
  <div class="card-body">
    <p><b>Address: &nbsp;</b><?php echo $user['address']; ?></p>
    <p><b>Email: &nbsp;</b><?php echo $user['email']; ?></p></p>
    <p><b>Phone number: &nbsp;</b><?php echo $user['phone']; ?></p>
    <p><b>Subjects: &nbsp;</b>
    <?php $resultstr = array(); ?>
    <?php foreach($predmeti as $predmet) : ?>
      <?php if($user['name'] == $predmet['teacher']) : ?>
        <?php array_push($resultstr, $predmet['subject']); ?>
      <?php endif; ?>
    <?php endforeach; ?>
    <?php echo implode(", ",$resultstr); ?>
    </p>
  </div>
</div>
</div>
<?php endif; ?>