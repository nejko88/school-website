<div class="col-md-6 offset-md-3">
<?php if (!empty($students)) : ?>

<?php if ($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'teacher') : ?>
<h1>Students</h1><br>

<table class="table table-hover table-bordered">
  <thead>
    <tr class="table-primary">
      <th scope="col">Name</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($students as $student) : ?>
    <tr class="table-light">
      <th scope="row"><a class="black" href="<?php echo base_url().'users/'.$student['id']; ?>"><?php echo $student['name']; ?></a>
      </th>
    </tr>
    <?php endforeach; ?>  
  </tbody>
</table>
<br>
<?php endif; ?>
<?php endif; ?>

<?php if (!empty($teachers)) : ?>

<h1>Teachers</h1><br>

<table class="table table-hover table-bordered">
  <thead>
    <tr class="table-primary">
      <th scope="col">Name</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($teachers as $teacher) : ?>
    <tr class="table-light">
      <th scope="row"><a class="black" href="<?php echo base_url().'users/'.$teacher['id']; ?>"><?php echo $teacher['name']; ?></a>
      </th>
    </tr>
    <?php endforeach; ?>  
  </tbody>
</table> 

<?php endif; ?>
</div>