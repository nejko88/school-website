<?php if($role == 'student') : ?>

<div class="card border-primary mb-3">
  <div class="card-header"><b><?php echo $name.', '.$class; ?></b><a class="btn btn-primary btn-right" href="<?php echo base_url().'schedules/classes/'.$class_id; ?>">Schedule</a></div>
</div>
<br>

<h3>Grades</h3><br>
	<table class="table table-bordered table-grades">
		<thead>
			<tr class="table-active">
				<th>Subject</th>
				<th>1. semester</th>
				<th>2. semester</th>
        <th>3. semester</hd>
        <th>Average</th>
			</tr>
		</thead>
		<tbody>
    <?php foreach($subjects as $subject) : ?>
    <tr class="table-light">
      <th scope="row"><?php echo ucfirst($subject); ?></th>
      <?php $j = 0; $k = 0; ?>
      <?php for ($i=1; $i < 4; $i++) : ?>
      <td class="grade">
      <?php $l = 0; ?>
      <?php foreach($grades as $grade) : ?>
      <?php if ($grade['subject'] == $subject && $grade['semester'] == $i) { 
        echo $grade['grade'].'&nbsp;&nbsp;';
        $k += $grade['grade'];
        $j += 1;
        $l += 1;
      } ?>
      <?php endforeach; ?>
      <?php echo ($l == 0 ? '/' : ''); ?>
      </td>
      <?php endfor; ?>
      <td><?php echo ($j != 0 ? round($k/$j, 1) : '/'); ?></td>
    </tr>
    <?php endforeach; ?>
		</tbody>
	</table>
<br>
<div style="font-size:18px;" class="alert alert-dismissible alert-primary">
  Your average grade is: &nbsp;<strong><?= $avg; ?></strong>.
</div>
<br>
<div class="card border-primary mb-3">
  <div class="card-header"><b>Class Exams</b></div>
  <div class="card-body">
    <?php foreach($c_exams as $exam) : ?>
    <div class="alert alert-warning">
    <b><?php echo date('d. m. Y', strtotime($exam['date'])); ?>, &nbsp;</b><?php echo $exam['subject']; ?> <b><span style="float:right;"><?php echo $exam['teacher']; ?></span> </b>
    </div>
    <?php endforeach; ?>
  </div>
</div>
<br>
<div class="card border-primary mb-3">
  <div class="card-header"><b>My Exams</b></div>
  <div class="card-body">
    <?php foreach($m_exams as $exam) : ?>
    <div class="alert alert-warning">
    <b><?php echo date('d. m. Y', strtotime($exam['date'])); ?>, &nbsp;</b><?php echo $exam['subject']; ?>
    <a style="float:right; display:table-cell; vertical-align:middle;" href="<?php echo site_url('exams/delete/'.$exam['id']); ?>">
      <i class="material-icons">clear</i>
    </a>
    <b><span style="float:right; margin-right:20px;"><?php echo $exam['teacher']; ?></span></b>
    </div>
    <?php endforeach; ?>
  </div>
</div>
<script>
  document.getElementById("navbar").style.minWidth="749px";
  document.getElementsByClassName("container")[0].style.minWidth="729px";
</script>
<?php endif; ?>

<?php if($role == 'teacher') : ?>
<div class="col-md-8 offset-md-2">
<h2><?php echo $name; ?></h2><br>
<div class="card border-primary mb-3">
  <div class="card-header"><b>Exams</b><a class="btn btn-primary btn-right" href="<?php echo base_url().'schedules/teachers/'.$id; ?>">Schedule</a></div>
  <div class="card-body">
    <?php foreach($t_exams as $exam) : ?>
    <div class="alert alert-warning">
    <b><?php echo date('d. m. Y', strtotime($exam['date'])); ?>, &nbsp;</b><?php echo $exam['subject']; ?> 
    <a style="float:right; display:table-cell; vertical-align:middle;" href="<?php echo site_url('exams/delete/'.$exam['id']); ?>">
      <i class="material-icons">clear</i>
    </a>
    <b><span style="float:right;  margin-right:20px;"><?php echo $exam['class']; ?></span></b>
    </div>
    <?php endforeach; ?>
  </div>
</div>
</div>
<script>
  document.getElementById("navbar").style.minWidth="450px";
  document.getElementsByClassName("container")[0].style.minWidth="450px";
</script>
<?php endif; ?>