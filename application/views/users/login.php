<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open('login'); ?>
<div class="row">
    <div class="col-md-4 offset-md-4">
        <h2 class="text-center"><?php echo $title; ?></h2><br>
        <div class="form-group required">
            <label>Name</label>
            <input type="text" name="name" class="form-control required" placeholder="Name" autofocus>
        </div>
        <div class="form-group required">
            <label>Password</label>
            <input type="password" name="password" class="form-control required" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary btn-block">Login</button>
    </div>
</div>
<?php echo form_close(); ?>
Added to login