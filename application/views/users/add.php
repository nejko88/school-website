<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('users/add'); ?>
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group">
	<label>Name</label>
	<input type="text" class="form-control" name="name" placeholder="Name" maxlength="28">
</div>
<div class="form-group">
	<label>Email</label>
	<input type="email" class="form-control" name="email" placeholder="Email">
</div>
<div class="form-group">
	<label>Password</label>
	<input type="text" class="form-control" name="password" placeholder="Password">
</div>
<div class="form-group radio">
	<label>Class</label>
<select class="custom-select" name="class">
	<option value="" selected></option>
    <?php foreach($classes as $class) : ?>
    <option value="<?php echo $class['class']; ?>"><?php echo $class['class']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<div class="form-group radio">
	<label>Role</label>
<select class="custom-select" name="role">
	<option value="student">Student</option>
    <option value="teacher">Teacher</option>
    <option value="admin">Administrator</option>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Add User</button>
</div>
</div>

<?php echo form_close(); ?>
