<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('users/update/'.$user['role']); ?>
<input type="hidden" name="id" value="<?php echo $user['id'];?>">
<input type="hidden" name="role" value="teacher">
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group">
	<label>Name</label>
	<input type="text" class="form-control" value="<?php echo $user['name'];?>" name="name" placeholder="Name" maxlength="28">
</div>
<div class="form-group">
	<label>Address</label>
	<input type="text" class="form-control" value="<?php echo $user['address'];?>" name="address" placeholder="Address" maxlength="28">
</div>
<div class="form-group">
	<label>Email</label>
	<input type="email" class="form-control" value="<?php echo $user['email'];?>" name="email" placeholder="Email">
</div>
<div class="form-group">
	<label>Phone Number</label>
	<input type="text" class="form-control" value="<?php echo $user['phone'];?>" name="phone" placeholder="Phone number">
</div>
<button type="submit" class="btn btn-primary btn-block">Edit Teacher</button>
</div>
</div>

<?php echo form_close(); ?>
