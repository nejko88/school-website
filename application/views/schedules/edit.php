<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo $link = array_reverse(explode('/', $_SERVER['REQUEST_URI']))[0]; ?>
<?php echo $string = array_reverse(explode('/', $_SERVER['REQUEST_URI']))[2]; ?>

<?php echo form_open_multipart('schedules/update/'.$string.'/'.$schedule['id'].'/'.$link); ?>
<input type="hidden" name="id" value="<?php echo $schedule['id'];?>">
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group radio">
	<label>Day</label>
<select class="custom-select" name="day">
    <option value="monday-1" <?php echo ($schedule['day_id'] == 1) ? 'selected' : ''; ?>>Monday</option>
    <option value="tuesday-2" <?php echo ($schedule['day_id'] == 2) ? 'selected' : ''; ?>>Tuesday</option>
    <option value="wednesday-3" <?php echo ($schedule['day_id'] == 3) ? 'selected' : ''; ?>>Wednesday</option>
    <option value="thursday-4" <?php echo ($schedule['day_id'] == 4) ? 'selected' : ''; ?>>Thursday</option>
    <option value="friday-5" <?php echo ($schedule['day_id'] == 5) ? 'selected' : ''; ?>>Friday</option>
</select>
</div>
<div class="form-group radio">
	<label>Time</label>
<select class="custom-select" name="time">
    <?php for ($i=7; $i < 17; $i++)  : ?>
    <option value="<?php echo $i; ?>" <?php echo ($schedule['time'] == $i) ? 'selected' : ''; ?>><?php echo $i.' - '.($i+1); ?></option>
    <?php endfor; ?>
</select>
</div>
<div class="form-group radio">
	<label>Subject</label>
<select class="custom-select" name="subject">
    <?php foreach($subjects as $subject) : ?>
    <option value="<?php echo $subject['subject'].' - '.$subject['teacher']; ?>" <?php echo ($schedule['subject'] == $subject['subject'] && $schedule['teacher'] == $subject['teacher']) ? 'selected' : ''; ?>><?php echo $subject['subject'].' - '.$subject['teacher']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<div class="form-group radio">
	<label>Class</label>
<select class="custom-select" name="class">
    <?php foreach($classes as $class) : ?>
    <option value="<?php echo $class['class']; ?>" <?php echo ($schedule['class'] == $class['class']) ? 'selected' : ''; ?>><?php echo $class['class']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<div class="form-group radio">
	<label>Classroom</label>
<select class="custom-select" name="room">
    <?php foreach($rooms as $room) : ?>
    <option value="<?php echo $room['room']; ?>" <?php echo ($schedule['room'] == $room['room']) ? 'selected' : ''; ?>><?php echo $room['room']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Edit Lesson</button>
</div>
</div>

<?php echo form_close(); ?>