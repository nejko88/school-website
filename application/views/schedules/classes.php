<h1 align="center"><?= $title; ?></h1><br>

<table class="table table-schedule table-bordered">
  <thead>
    <tr class="table-primary">
      <th scope="col"></th>
      <th scope="col">Monday</th>
      <th scope="col">Tuesday</th>
      <th scope="col">Wednsday</th>
      <th scope="col">Thursday</th>
      <th scope="col">Friday</th>
    </tr>
  </thead>
  <tbody>
    <?php for ($i=7; $i < 17; $i++)  : ?>
    <tr class="table-light">
      <th class="time"><?php echo $i.' - '.($i+1); ?></th>
      <?php for ($j=1; $j < 6; $j++) : ?>
      <td style="padding:0px;">
      <div class="container" style="padding:0px;">
      <?php foreach($schedules as $schedule) : ?>
      <?php if ($schedule['time'] == $i && $schedule['day_id'] == $j) {
      echo '<div class="row" style="display: inline-block; width:100%;">';
      echo '<span class="default" style="float:left; margin-left:2px">';
      echo explode(' ',$schedule['teacher'])[1];
      echo '</span>';
      echo '<span class="default" style="float:right; margin-right:2px">';
      echo $schedule['room'];
      echo '</span>';
      echo '</div>';
      echo '<div class="row" style="display: inline-block; width:100%;">';
      echo '<span class="default" style="float:left; margin-left:2px">';
      echo $schedule['subject'];
      echo '</span>';
      ?>
      <?php if ($this->session->userdata('role') == 'admin') : ?>
      <span class="default" style="float:right; margin-right:2px">
      <a href="<?php echo site_url('schedules/edit/classes/'.$schedule['id'].'/'.$id); ?>">
      <i class="material-icons">mode_edit</i>
      </a>
      <a href="<?php echo site_url('schedules/delete/classes/'.$schedule['id'].'/'.$id); ?>">
      <i class="material-icons">clear</i>
      </a>
      </span>
      <?php endif; ?>
      </div>
      <?php };?>
      <?php endforeach; ?>
      </div>
      </td>
      <?php endfor; ?>
    </tr>
    <?php endfor; ?>  
  </tbody>
</table> 

<script>
  document.getElementById("navbar").style.minWidth="900px";
  document.getElementsByClassName("container")[0].style.minWidth="900px";
</script>