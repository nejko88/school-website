<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('schedules/add'); ?>
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group radio">
	<label>Day</label>
<select class="custom-select" name="day">
    <option value="monday-1">Monday</option>
    <option value="tuesday-2">Tuesday</option>
    <option value="wednesday-3">Wednesday</option>
    <option value="thursday-4">Thursday</option>
    <option value="friday-5">Friday</option>
</select>
</div>
<div class="form-group radio">
	<label>Time</label>
<select class="custom-select" name="time">
    <?php for ($i=7; $i < 17; $i++)  : ?>
    <option value="<?php echo $i; ?>"><?php echo $i.' - '.($i+1); ?></option>
    <?php endfor; ?>
</select>
</div>
<div class="form-group radio">
	<label>Subject</label>
<select class="custom-select" name="subject">
    <?php foreach($subjects as $subject) : ?>
    <option value="<?php echo $subject['subject'].' - '.$subject['teacher']; ?>"><?php echo $subject['subject'].' - '.$subject['teacher']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<div class="form-group radio">
	<label>Class</label>
<select class="custom-select" name="class">
    <?php foreach($classes as $class) : ?>
    <option value="<?php echo $class['class']; ?>"><?php echo $class['class']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<div class="form-group radio">
	<label>Classroom</label>
<select class="custom-select" name="room">
    <?php foreach($rooms as $room) : ?>
    <option value="<?php echo $room['room']; ?>"><?php echo $room['room']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Add Lesson</button>
</div>
</div>

<?php echo form_close(); ?>