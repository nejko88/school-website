<div class="col-md-4 offset-md-4">
<h1><?= 'Schedules'; ?></h1><br>

<table class="table table-bordered table-hover">
  <thead>
    <tr class="table-primary">
      <th scope="col"><?= ucfirst($title); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($strings as $string) : ?>
    <tr class="table-light">
      <th scope="row">
      <a class="black" href="<?php echo base_url().'schedules/'.$title.'/'.$string['id']; ?>"><?php echo $string['name']; ?>
      </a>
      </th>
    </tr>
    <?php endforeach; ?>  
  </tbody>
</table>
</div>