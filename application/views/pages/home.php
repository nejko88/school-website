<h1><?= $title; ?></h1><br>
<br><div style="font-size:18px;">
<div><b>This is a simple school webpage where administrators, students and teachers have different privileges.</b></div><br>

<div><b>Log in to see for yourself.</b></div><br>

<div>Anyone can see the school's schedule and teachers' information.</div><br>

<div>Students can add their exams and check out their grades and exam dates on their profile.<br><b>Name: </b>Matej Mulej, <b>Password: </b>student</div><br>

<div>Teachers can add class exams, grade students and see students' information.<br><b>Name: </b>Daniel Bezjak, <b>Password: </b>teacher</div><br>

<div>Administrators can add, edit and delete students, teachers, classes, subjects and schedules.<br><b>Name: </b>admin, <b>Password: </b>admin</div><br>

<div>
Added to home page
</div>
</div>
