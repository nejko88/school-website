<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('exams/add_c'); ?>
<input type="hidden" name="teacher" value="<?php echo $name; ?>">
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group">
	<label>Date</label><br>
	<input class="form-control" type="date" name="date">
</div>
<div class="form-group radio">
	<label>Class</label>
<select class="custom-select" name="class">
    <?php foreach($classes as $class) : ?>
    <option value="<?php echo $class['class']; ?>"><?php echo $class['class']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<div class="form-group radio">
<label>Subject</label>
<select class="custom-select" name="subject">
<?php foreach($subjects as $subject) : ?>
    <?php if($name == $subject['teacher']) : ?>
    <option value="<?php echo $subject['subject']; ?>"><?php echo ucfirst($subject['subject']); ?></option>
    <?php endif; ?>
<?php endforeach; ?>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>
</div>

<?php echo form_close(); ?>