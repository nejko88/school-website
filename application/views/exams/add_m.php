<?php echo validation_errors('<p class="alert alert-danger">', '</p>'); ?>

<?php echo form_open_multipart('exams/add_m'); ?>
<input type="hidden" name="student" value="<?php echo $name; ?>">
<input type="hidden" name="class" value="<?php echo $class; ?>">
<div class="row">
<div class="col-md-4 offset-md-4">
<h2 class="text-center"><?php echo $title; ?></h2><br>
<div class="form-group">
	<label>Date</label><br>
	<input class="form-control" type="date" name="date">
</div>
<div class="form-group radio">
	<label>Subject</label>
<select class="custom-select" name="subject">
    <?php foreach($subjects as $subject) : ?>
    <option value="<?php echo $subject['subject'].' - '.$subject['teacher']; ?>"><?php echo ucfirst($subject['subject']).' - '.$subject['teacher']; ?></option>
    <?php endforeach; ?>
</select>
</div>
<button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>
</div>

<?php echo form_close(); ?>