<div style="width:729px;">
<form id="myForm" action="" class="form-inline" method="post">
  <div class="form-group">
    <div style="display:inline-block;">
    <label for="name" class="label-control col-md-2">Class:</label>
    </div>
    <div style="display:inline-block">
		<select class="custom-select" name="class">
      <?php foreach($classes as $class) : ?>
      <option value="<?php echo $class['class']; ?>"><?php echo $class['class']; ?></option>
      <?php endforeach; ?>
    </select>
    </div>
	</div>
  <div class="form-group">
    <div style="display:inline-block;  margin-left:80px">
    <label for="name" class="label-control col-md-2">Subject:</label>
    </div>
    <div style="display:inline-block;  margin-left:-10px">
		<select class="custom-select" name="subject">
      <?php foreach($subjects as $subject) : ?>
      <option value="<?php echo $subject; ?>"><?php echo $subject; ?></option>
      <?php endforeach; ?>
    </select>
    </div>
	</div>
  <button style="margin-left:20px" type="button" id="btnSave" class="btn btn-primary">Find</button>
</form>
</div>

<br><h3 id="class"></h3><br>
<div class="alert alert-success" style="display: none;"></div>

<table id="table-grades" class="table table-bordered table-grades" style="margin-top: 20px;">
  <thead>
    <tr  class="table-active">
      <th>Student</th>
      <th>1. semester</th>
      <th>2. semester</th>
      <th>3. semester</hd>
      <th>Average</th>
      <th></th>
    </tr>
  </thead>
  <tbody id="showdata">
    
  </tbody>
</table>

<div id="myModal" class="modal fade col-md-4 offset-md-4" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Grade</h4>
      </div>
      <div class="modal-body">
        <form id="addGrade" style="min-width:100%;" action="" method="post">
          <div class="form-group">
            <label class="label-control">Student:</label>
            <div>
              <input type="text" name="student" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="label-control">Subject:</label>
            <div>
              <input type="text" name="subject" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="label-control">Semester:</label>
            <div>
              <select class="custom-select" name="semester">
                <option value="1">1. semester</option>
                <option value="2">2. semester</option>
                <option value="3">3. semester</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="label-control">Grade:</label>
            <div>
              <select class="custom-select" name="grade">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnAdd" class="btn btn-success mr-auto">Add Grade</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  document.getElementById("table-grades").style.minWidth="729px";
  document.getElementById("navbar").style.minWidth="744px";
  document.getElementsByClassName("container")[0].style.minWidth="729px";
	$(function(){
    if (localStorage.getItem("predmet") != null) {
      a = localStorage.getItem("predmet");
      $('select[name=subject]').val(a);
    }
    if (localStorage.getItem("razred") != null) {
      b = localStorage.getItem("razred");
      $('select[name=class]').val(b);
    }

		showSomeStudents();

    //Add Grade
    $('#showdata').on('click', '.item-add', function(){
      $('#myModal').modal('show');
      var student = $(this).attr('data-student');
			var subject = $(this).attr('data-subject');
      $('input[name=student]').val(student);
			$('input[name=subject]').val(subject);
      $('#addGrade').attr('action', '<?php echo base_url() ?>grades/add');
    });

    $('#btnAdd').click(function(){
      var url = $('#addGrade').attr('action');
			var data = $('#addGrade').serialize();

      $.ajax({
					type: 'ajax',
					method: 'post',
					url: url,
					data: data,
					async: false,
					dataType: 'json',
					success: function(response){
						if(response.success){
							$('#myModal').modal('hide');
							$('#addGrade')[0].reset();
              $('.alert-success').html('Grade added successfully').fadeIn().delay(4000).fadeOut('slow');
							showSomeStudents();
						}else{
							alert('Error');
						}
					},
					error: function(){
						alert('Could not add data');
					}
				});
    });
		//Show Some Students
    $('#btnSave').click(function(){
      showSomeStudents();
      predmet = $('select[name=subject]').val();
      razred = $('select[name=class]').val();
      localStorage.setItem("predmet", predmet);
      localStorage.setItem("razred", razred);
    });
    
    function showSomeStudents(){
      $('#myForm').attr('action', '<?php echo base_url() ?>grades/showSomeStudents');
			var url = $('#myForm').attr('action');
			var data = $('#myForm').serialize();

      $.ajax({
        type: 'ajax',
        method: 'post',
        url: url,
        data: data,
        async: false,
        dataType: 'json',
        success: function(data2){
          var html = '';
          var i;
          for(i=0; i<data2[0].length; i++){
            html +='<tr class="table-light">'+
              '<th>'+data2[0][i].name+'</th>';

            sum = 0;
            num = 0;
            for (j = 1; j < 4; j++) {
              html += '<td>';

              grade = '/';
              ocena = '';
              vsota = 0;
              for (k = 0; k < data2[2].length; k++) {
                if (data2[2][k].student ==  data2[0][i].name && data2[2][k].subject == data2[1] && data2[2][k].semester == j) {
                  grade = data2[2][k].grade;
                  ocena += grade+'&nbsp; ';
                }
              }
              if (grade == '/') {
                html += grade;
                len = 0;
              }
              else {
                html += ocena.trim();
                res = ocena.trim().split('&nbsp; ');
                len = res.length;
                for (l = 0; l < res.length; l++) {
                  vsota += parseInt(res[l]);
                }
              }
              html += '</td>';
              num += len;
              sum += vsota;
            }
            avg = Math.round( sum/num * 10 ) / 10;
            if(isNaN(avg)) { avg = '/'; }

            html +='<td>'+avg+'</td>';
            html +='<td style="width:150px;" align="center">'+
            '<a href="javascript:;" style="color:black" class="btn btn-secondary btn-left item-add" data-student="'+data2[0][i].name+'" data-subject="'+data2[1]+'"><i class="material-icons">add</i></a><a style="color:black" class="btn btn-right btn-secondary" href="<?php echo site_url('grades/view/'); ?>'+data2[1]+'/'+data2[0][i].name+'"><i class="material-icons">content_paste</i></a>'+
            '</td>'+
              '</tr>';
          }
          $('#showdata').html(html);
          $('#class').html(data2[1]);
        },
        error: function(){
          alert('Could not get Data from Database');
        }
      });
		}
	});
</script>