<div class="alert alert-success" style="display: none;"></div>
<br>
<div style="text-align: center;">
<h2 style="width:auto"><?= $grades[0]['student'].' - '.$grades[0]['subject']; ?></h2>
</div>
<br>
<div class="col-md-4 offset-md-4">
<br>

<table class="table table-grade table-bordered">
  <thead>
    <tr class="table-active row">
      <th class="col-4">Semester</th>
      <th class="col-4">Grade</th>
      <th class="col-4"></th>
    </tr>
  </thead>
  <tbody id="showdata">
    
  </tbody>
</table>
<br>
<div style="text-align: center">
<a href="<?php echo base_url(); ?>grades" class="btn btn-outline-primary btn-large">Back</a>
</div>

<div id="myModal" class="modal fade col-md-4 offset-md-4" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form id="myForm" action="" method="post">
        <input type="hidden" name="id" value="0">
          <div class="form-group">
            <label class="label-control">Student:</label>
            <div>
              <input type="text" name="student" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="label-control">Subject:</label>
            <div>
              <input type="text" name="subject" class="form-control" readonly>
            </div>
          </div>
          <div class="form-group">
            <label class="label-control">Semester:</label>
            <div>
              <select class="custom-select" name="semester">
                <option value="1">1. semester</option>
                <option value="2">2. semester</option>
                <option value="3">3. semester</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="label-control">Grade:</label>
            <div>
              <select class="custom-select" name="grade">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" class="btn btn-success mr-auto">Edit Grade</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="deleteModal" class="modal fade col-md-4 offset-md-4" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnDelete" class="btn btn-danger mr-auto">Delete Grade</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(function(){
		showAllGrades();
    
    $('#btnSave').click(function(){
			var url = $('#myForm').attr('action');
			var data = $('#myForm').serialize();

      $.ajax({
        type: 'ajax',
        method: 'post',
        url: url,
        data: data,
        async: false,
        dataType: 'json',
        success: function(response){
          if(response.success){
            $('#myModal').modal('hide');
            $('#myForm')[0].reset();
            $('.alert-success').html('Grade edited successfully').fadeIn().delay(4000).fadeOut('slow');
            showAllGrades();
          }else{
            alert('Error');
          }
        },
        error: function(){
          alert('Could not edit data');
        }
      });
		});

    //edit
		$('#showdata').on('click', '.item-edit', function(){
			var id = $(this).attr('data-id');
      var student = $(this).attr('data-student');
			var subject = $(this).attr('data-subject');
			$('#myModal').modal('show');
			$('#myModal').find('.modal-title').text('Edit Grade');
			$('#myForm').attr('action', '<?php echo base_url(); ?>grades/update');
			$.ajax({
				type: 'ajax',
				method: 'get',
				url: '<?php echo base_url(); ?>grades/edit/'+subject+'/'+student,
				data: {id: id},
				async: false,
				dataType: 'json',
				success: function(data){
					$('input[name=student]').val(data.student);
					$('input[name=subject]').val(data.subject);
          $('select[name=semester]').val(data.semester);
          $('select[name=grade]').val(data.grade);
					$('input[name=id]').val(data.id);
				},
				error: function(){
					alert('Could not edit grade');
				}
			});
		});

    //delete
		$('#showdata').on('click', '.item-delete', function(){
			var id = $(this).attr('data-id');
			$('#deleteModal').modal('show');

			//prevent previous handler - unbind()
			$('#btnDelete').unbind().click(function(){
				$.ajax({
					type: 'ajax',
					method: 'get',
					async: false,
					url: '<?php echo base_url() ?>grades/delete',
					data: {id:id},
					dataType: 'json',
					success: function(response){
						if(response.success){
							$('#deleteModal').modal('hide');
							$('.alert-success').html('Grade deleted successfully').fadeIn().delay(4000).fadeOut('slow');
							showAllGrades();
						}
						else{
							alert('Error');
						}
					},
					error: function(){
						alert('Error deleting');
					}
				});
			});
		});

    //show all
    function showAllGrades(){
			$.ajax({
				type: 'GET',
				url: '<?php echo site_url() ?>grades/showAllGrades/<?= $grades[0]['subject'].'/'.$grades[0]['student']; ?>',
				async: false,
        contentType: "application/json; charset=utf-8",
				dataType: 'json',
				success: function(data){
					var html = '';
					var i;
					for(i=0; i<data.length; i++){
						html +='<tr class="row">'+
									'<td class="col-4">'+data[i].semester+'.</td>'+
									'<td class="col-4">'+data[i].grade+'</td>'+
									'<td class="col-4">'+
                  '<a class="btn-left item-edit" href="javascript:;" data-id="'+data[i].id+'" data-student="'+data[i].student+'" data-subject="'+data[i].subject+'" data-grade="'+data[i].grade+'" data-semester="'+data[i].semester+'"><i class="material-icons">mode_edit</i></a>'+
                  '<a class="btn-right item-delete" href="javascript:;" data-id="'+data[i].id+'"><i class="material-icons">clear</i></a>'+
                  '</td>'+
							    '</tr>';
					}
					$('#showdata').html(html);
				},
				error: function(){
					alert('Could not get data from database');
				}
			});
		}
  });
</script>