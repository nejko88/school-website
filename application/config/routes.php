<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['grades'] = 'grades/index';

$route['subjects'] = 'subjects/index';
$route['classes'] = 'classes/index';
$route['classes/add'] = 'classes/add';
$route['classes/(:any)'] = 'classes/view/$1';

$route['schedules/add'] = 'schedules/add';
$route['schedules/teachers'] = 'schedules/index/teachers';
$route['schedules/classes'] = 'schedules/index/classes';
$route['schedules/(:any)'] = 'schedules/view/$1';

$route['users/search'] = 'users/search';
$route['users/add_teacher'] = 'users/add_teacher';
$route['users/add_student'] = 'users/add_student';
$route['users/students'] = 'users/index/student';
$route['users/teachers'] = 'users/index/teacher';
$route['users/logout'] = 'users/logout';
$route['users/(:any)'] = 'users/view/$1';
$route['users'] = 'users/index/all';

$route['profile'] = 'users/profile';
$route['login'] = 'users/login';

$route['default_controller'] = 'pages/view';
$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
