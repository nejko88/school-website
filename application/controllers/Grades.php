<?php
class Grades extends CI_controller{
  function __construct(){
    parent:: __construct();
  }

  function index(){
		if($this->session->userdata('role') != 'teacher'){
			redirect('home');
		}
    $data['classes'] = $this->class_model->get_classes();
		$array = $this->subject_model->get_subjects();

		$data['subjects'] = array();
		for ($i=0; $i < count($array); $i++) { 
			if (!in_array($array[$i]['subject'], $data['subjects'])) {
				$data['subjects'][$i] = $array[$i]['subject'];
			}
		}

		$this->load->view('templates/header');
		$this->load->view('grades/index', $data);
		$this->load->view('templates/footer');
  }

  function view($subject, $student){
		if($this->session->userdata('role') != 'teacher'){
			redirect('home');
		}
		$data['grades'] = $this->grade_model->get_grade($subject, $student);

		if (empty($data['grades'])) {
			$this->load->view('templates/header');
			$this->load->view('errors/grades');
			$this->load->view('templates/footer');
		}
		else {
			$this->load->view('templates/header');
			$this->load->view('grades/view', $data);
			$this->load->view('templates/footer');
		}
  }
  
  public function showSomeStudents(){
	if($this->session->userdata('role') != 'teacher'){
		redirect('home');
	}
    $result = $this->grade_model->showSomeStudents();
    $subject = $this->input->post('subject');
    $grades = $this->grade_model->get_grades();
	echo json_encode(array($result, $subject, $grades));
  }

  public function add(){
		if($this->session->userdata('role') != 'teacher'){
			redirect('home');
		}
		$result = $this->grade_model->add_grade();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
  }
  
 public function showAllGrades($subject, $student){
		if($this->session->userdata('role') != 'teacher'){
			redirect('home');
		}
		$result = $this->grade_model->showAllGrades($subject, $student);
		echo json_encode($result);
  } 
  
  public function edit($subject, $student){
		if($this->session->userdata('role') != 'teacher'){
			redirect('home');
		}
		$result = $this->grade_model->edit($subject, $student);
		echo json_encode($result);
  }
  
  public function update(){
		if($this->session->userdata('role') != 'teacher'){
			redirect('home');
		}
		$result = $this->grade_model->update();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
  }
  
  public function delete(){
		if($this->session->userdata('role') != 'teacher'){
			redirect('home');
		}
		$result = $this->grade_model->delete();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
}