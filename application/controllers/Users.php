<?php
class Users extends CI_controller{
	function __construct()
    {
        parent::__construct();
    }

	public function search(){
		$data = $this->session->all_userdata();

		$keyword = $this->input->post('keyword');		
		$query = $this->user_model->search($keyword);

		$data['students'] = $query['students'];
		$data['teachers'] = $query['teachers'];

		if (empty($data['students']) && empty($data['teachers'])) {
			$this->load->view('templates/header');
			$this->load->view('templates/footer');
		}
		else {
			$this->load->view('templates/header');
			$this->load->view('users/search', $data);
			$this->load->view('templates/footer');
		}
	}

	public function index($role){
		if($this->session->userdata('role') == ''){
			redirect('login');
		}
		if($this->session->userdata('role') == 'student'){
			redirect('home');
		}
		if ($role != 'teacher') {
			$this->load->view('templates/header');
			$this->load->view('errors/error');
			$this->load->view('templates/footer');
		}
		else{
			$data['title'] = $role;

			$data['users'] = $this->user_model->get_users($role);
	
			$this->load->view('templates/header');
			$this->load->view('users/index', $data);
			$this->load->view('templates/footer');
		}
	}

	public function view($id){
		$data['user'] = $this->user_model->get_user($id);
		$data['classes'] = $this->class_model->get_classes();

		if($this->session->userdata('role') == 'student' && $this->session->userdata('role') == '' && $data['user']['role'] != 'teacher'){
			redirect('home');
		}

		for ($i=0; $i < count($data['classes']); $i++) { 
			if ($data['classes'][$i]['class'] == $data['user']['class']) {
				$data['class_id'] = $data['classes'][$i]['id'];
			}
		}

		$data['predmeti']= $this->subject_model->get_subjects();
		$data['subjects'] = array();
		for ($i=0; $i < count($data['predmeti']); $i++) {
			if (!in_array($data['predmeti'][$i]['subject'], $data['subjects'])) {
				array_push($data['subjects'], $data['predmeti'][$i]['subject']) ;
			}
		}
		sort($data['subjects']);
		
		$sum = 0;

		$data['grades'] = $this->user_model->get_grades($data['user']['name']);
		foreach ($data['grades'] as $grade) {
			$sum += $grade['grade'];
		}
		if (count($data['grades']) == 0) {
			$data['avg'] = 0;
		}
		else {
			$data['avg'] = round($sum/count($data['grades']), 1);
		}

		if (empty($data['user'])) {
			$this->load->view('templates/header');
			$this->load->view('errors/error');
			$this->load->view('templates/footer');
		}

		else{
			$this->load->view('templates/header');
			$this->load->view('users/view', $data);
			$this->load->view('templates/footer');
		}
	}

    // Add user
	public function add_teacher(){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}
		
		$data['title'] = 'Add Teacher';

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header');
			$this->load->view('users/add_teacher', $data);
			$this->load->view('templates/footer');
		}
		else{
			// Encrypt password
			$enc_password = md5($this->input->post('password'));
            $this->user_model->add($enc_password);
            
            // Set message
            $this->session->set_flashdata('user_registered', 'Teacher has been added');

			redirect('users/add_teacher');
		}
	}

	public function add_student(){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}

		$data['title'] = 'Add Student';
		$data['classes'] = $this->class_model->get_classes();

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header');
			$this->load->view('users/add_student', $data);
			$this->load->view('templates/footer');
		}
		else{
			// Encrypt password
			$enc_password = md5($this->input->post('password'));
            $this->user_model->add($enc_password);
            
            // Set message
            $this->session->set_flashdata('user_registered', 'Student has been added');

			redirect('users/add_student');
		}
	}
	
	public function delete($id, $role) {
		$this->user_model->delete_user($id);

		// Set message
		$this->session->set_flashdata('user_deleted', ucfirst($role).' has been deleted');
		redirect('users/'.$role.'s');
	}

	public function edit_teacher($id){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}

		$data['user'] = $this->user_model->get_user($id);

		if (empty($data['user'])) {
			show_404();
		}

		$data['title'] = 'Edit Teacher';

		$this->load->view('templates/header');
		$this->load->view('users/edit_teacher', $data);
		$this->load->view('templates/footer');
	}

	public function edit_student($id){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}

		$data['user'] = $this->user_model->get_user($id);
		$data['classes'] = $this->class_model->get_classes();

		if (empty($data['user'])) {
			show_404();
		}

		$data['title'] = 'Edit Student';

		$this->load->view('templates/header');
		$this->load->view('users/edit_student', $data);
		$this->load->view('templates/footer');
	}

	public function update($role){
		$this->user_model->update_user();

		// Set message
		$this->session->set_flashdata('user_updated', ucfirst($role).' has been updated');

		redirect('users/'.$role.'s');
	}
    
    // Check if email exists
	public function check_email_exists($email){
		$this->form_validation->set_message('check_email_exists', 'This email has already been registered');

		if ($this->user_model->check_email_exists($email)) {
			return true;
		}
		else {
			return false;
		}
	}

    // Login user
    public function login(){
			if($this->session->userdata('role') != ''){
				redirect('home');
			}

	    $data['title'] = 'Log in';

			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');

			if ($this->form_validation->run() === FALSE) {
				$this->load->view('templates/header');
				$this->load->view('users/login', $data);
				$this->load->view('templates/footer');
			}
			else{
				// Get name
				$name = $this->input->post('name');
				// Get and encrypt password
				$password = md5($this->input->post('password'));

				// Login user
				$result = $this->user_model->login($name, $password);

				if ($result) {
					// Create session
					$user_data = array(
						'id' => $result['id'],
						'name' => $name,
						'role' => $result['role'],
						'email' => $result['email'],
						'class' => $result['class'],
						'address' => $result['address'],
						'phone' => $result['phone']
					);

					$this->session->set_userdata($user_data);

					// Set message
					$this->session->set_flashdata('user_loggedin', 'You are now loged in');
					
					if($this->session->userdata('role') == 'admin'){
						redirect('home');
					}
					
					redirect('profile');
				}
				else {
					// Set message
					$this->session->set_flashdata('login_failed', 'Login is invalid');
					redirect('login');
				}
			}
		}
		
    public function logout(){
			// Unset user data
      $array_session = array('role', 'name', 'id', 'email', 'class', 'address', 'phone');
			$this->session->unset_userdata($array_session);

			// Set message
      $this->session->set_flashdata('user_loggedout', 'You are now loged out');
			redirect('home');
    }
    
    public function profile(){
			// Check login
			if($this->session->userdata('role') == ''){
				redirect('login');
			}

			$data = $this->session->all_userdata();
			$data['classes'] = $this->class_model->get_classes();

			for ($i=0; $i < count($data['classes']); $i++) { 
				if ($data['classes'][$i]['class'] == $data['class']) {
					$data['class_id'] = $data['classes'][$i]['id'];
				}
			}

			$data['predmeti']= $this->subject_model->get_subjects();
			$data['subjects'] = array();
			for ($i=0; $i < count($data['predmeti']); $i++) {
				if (!in_array($data['predmeti'][$i]['subject'], $data['subjects'])) {
					array_push($data['subjects'], $data['predmeti'][$i]['subject']) ;
				}
			}
			sort($data['subjects']);
			$sum = 0;

			$data['grades'] = $this->user_model->get_grades($data['name']);
			foreach ($data['grades'] as $grade) {
				$sum += $grade['grade'];
			}
			if (count($data['grades']) == 0) {
				$data['avg'] = 0;
			}
			else {
				$data['avg'] = round($sum/count($data['grades']), 1);
			}
			
			if($this->session->userdata('role') == 'teacher'){
				$data['t_exams']= $this->exam_model->get_teacher_exams($data['name']);
			}
			if($this->session->userdata('role') == 'student'){
				$data['c_exams']= $this->exam_model->get_class_exams($data['class']);
				$data['m_exams']= $this->exam_model->get_student_exams($data['name']);
			}
			$this->load->view('templates/header', $data);
			$this->load->view('users/profile', $data);
			$this->load->view('templates/footer');
		}
}