<?php
class Schedules extends CI_controller{
	public function index($string){
		if ($string == 'classes') {
			$data['strings'] = $this->class_model->get_classes();
			for ($i=0; $i < count($data['strings']); $i++) { 
				$data['strings'][$i]['name'] = $data['strings'][$i]['class'];
				unset($data['strings'][$i]['class']);
			}
		}

		else if ($string == 'teachers') {
			$data['strings'] = $this->user_model->get_users('teacher');
		}
		else {
			$this->load->view('templates/header');
			$this->load->view('errors/error');
			$this->load->view('templates/footer');
		}
		$data['title'] = $string;

		$this->load->view('templates/header');
		$this->load->view('schedules/index', $data);
		$this->load->view('templates/footer');
	}

	public function classes($id){
		$data = $this->session->all_userdata();
		$data['schedules'] = $this->schedule_model->get_schedules_classes($id);
		$data['classes'] = $this->class_model->get_classes();

		for ($i=0; $i < count($data['classes']); $i++) { 
			if ($data['classes'][$i]['id'] == $id) {
				$data['title'] = $data['classes'][$i]['class'];
				$data['id'] = $data['classes'][$i]['id'];
			}
		}

		if (empty($data['title'])) {
			$this->load->view('templates/header');
			$this->load->view('errors/error');
			$this->load->view('templates/footer');
		}
		else{
			$this->load->view('templates/header');
			$this->load->view('schedules/classes', $data);
			$this->load->view('templates/footer');
		}
	}

	public function teachers($id){
		$data = $this->session->all_userdata();
		$data['schedules'] = $this->schedule_model->get_schedules_teachers($id);
		$data['teachers'] = $this->user_model->get_users('teacher');

		for ($i=0; $i < count($data['teachers']); $i++) { 
			if ($data['teachers'][$i]['id'] == $id) {
				$data['title'] = $data['teachers'][$i]['name'];
				$data['id'] = $data['teachers'][$i]['id'];
			}
		}

		if (empty($data['title'])) {
			$this->load->view('templates/header');
			$this->load->view('errors/error');
			$this->load->view('templates/footer');
		}
		else{
			$this->load->view('templates/header');
			$this->load->view('schedules/teachers', $data);
			$this->load->view('templates/footer');
		}
	}

	public function add(){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}
		$data['title'] = 'Add Lesson';
		$data['classes'] = $this->class_model->get_classes();
		$data['teachers'] = $this->user_model->get_teachers();
		$data['subjects'] = $this->subject_model->get_subjects();
		$data['rooms'] = $this->schedule_model->get_rooms();

		$this->form_validation->set_rules('subject', 'Subject', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header');
			$this->load->view('schedules/add', $data);
			$this->load->view('templates/footer');
		}

		else if ($this->schedule_model->add() == false ) {
			$this->load->view('templates/header', $data);
			$this->load->view('schedules/add', $data);
			$this->load->view('templates/footer');
			// Set message
			$this->session->set_flashdata('schedule_exists', 'Lesson already exists');
			redirect('schedules/add');
		}
		else {
			$this->session->set_flashdata('schedule_added', 'Lesson has been added');
			redirect('schedules/add');
		}
	}

	public function delete($string, $id, $link) {
		$this->schedule_model->delete_schedule($id);

		// Set message
		$this->session->set_flashdata('schedule_deleted', 'Lesson has been deleted');
		redirect('schedules/'.$string.'/'.$link);
	}

	public function edit($string, $id, $link){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}
		$data['classes'] = $this->class_model->get_classes();
		$data['subjects'] = $this->subject_model->get_subjects();
		$data['schedule'] = $this->schedule_model->get_schedule($id);
		$data['rooms'] = $this->schedule_model->get_rooms();

		if (empty($data['schedule'])) {
			show_404();
		}

		$data['title'] = 'Edit Lesson';

		$this->load->view('templates/header');
		$this->load->view('schedules/edit', $data);
		$this->load->view('templates/footer');
	}

	public function update($string, $id, $link){
		$this->schedule_model->update_schedule($id);

		if ($this->schedule_model->update_schedule($id) == false ) {
			// Set message
			$this->session->set_flashdata('schedule_exists', 'Lesson already exists');
			redirect('schedules/edit/'.$string.'/'.$id.'/'.$link);
		}
		else {
			// Set message
			$this->session->set_flashdata('schedule_updated', 'Lesson has been updated');
			redirect('schedules/'.$string.'/'.$link);
		}
	}
}