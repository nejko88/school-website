<?php
class Subjects extends CI_controller{
	public function index(){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}

		$data['title'] = 'Subjects';
		$data['subjects'] = $this->subject_model->get_subjects();

		$this->load->view('templates/header');
		$this->load->view('subjects/index', $data);
		$this->load->view('templates/footer');
	}

  public function add(){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}
		
		$data['title'] = 'Add Subject';

		$data['teachers'] = $this->user_model->get_teachers();

		$this->form_validation->set_rules('subject', 'Subject', 'required');

		if ($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('subjects/add', $data);
			$this->load->view('templates/footer');
		}
		else if ($this->subject_model->add() == false ) {
			$this->load->view('templates/header', $data);
			$this->load->view('classes/add', $data);
			$this->load->view('templates/footer');
			// Set message
			$this->session->set_flashdata('subject_exists', 'Subject already exists');
			redirect('subjects/add');
		}
		else {
			$this->session->set_flashdata('subject_added', 'Subject has been added');
			redirect('subjects/add');
		}
	}

	public function delete($id, $subject) {
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}
		$this->subject_model->delete_subject($id, $subject);

		// Set message
		$this->session->set_flashdata('subject_deleted', 'Subject has been deleted');
		redirect('subjects');
	}
}