<?php
class Classes extends CI_controller{
	public function index(){
		if($this->session->userdata('role') == 'student' || $this->session->userdata('role') == ''){
			redirect('home');
		}

		$data['title'] = 'Classes';

		$data['classes'] = $this->class_model->get_classes();

		$this->load->view('templates/header');
		$this->load->view('classes/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($id){
		if($this->session->userdata('role') == 'student' || $this->session->userdata('role') == ''){
			redirect('home');
		}

		$data['users'] = $this->class_model->get_users($id);
		$data['title'] = $this->class_model->get_class($id)['class'];

		if (empty($data['title'])) {
			$this->load->view('templates/header');
			$this->load->view('errors/error');
			$this->load->view('templates/footer');
		}
		else {
			$this->load->view('templates/header');
			$this->load->view('classes/view', $data);
			$this->load->view('templates/footer');
		}
	}

  public function add(){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}

		$data['title'] = 'Add Class';

		$this->form_validation->set_rules('class', 'Class', 'required');

		if ($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('classes/add', $data);
			$this->load->view('templates/footer');
		}
		else if ($this->class_model->add() == false ) {
			$this->load->view('templates/header', $data);
			$this->load->view('classes/add', $data);
			$this->load->view('templates/footer');
			// Set message
			$this->session->set_flashdata('class_exists', 'Class already exists');
			redirect('classes/add');
		}
		else {
			$this->session->set_flashdata('class_added', 'Class has been added');
			redirect('classes/add');
		}
	}

	public function delete($id, $class) {
		$this->class_model->delete_class($id, $class);

		// Set message
		$this->session->set_flashdata('class_deleted', 'Class has been deleted');
		redirect('classes');
	}

	public function edit($id){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}
		
		$data['class'] = $this->class_model->get_class($id);

		if (empty($data['class'])) {
			$this->load->view('templates/header');
			$this->load->view('errors/error');
			$this->load->view('templates/footer');
		}

		else {
			$data['year'] = explode(' ', $data['class']['class'])[0];
			$data['letter'] = explode(' ', $data['class']['class'])[1];

			if (empty($data['class'])) {
				show_404();
			}

			$data['title'] = 'Edit '.$data['class']['class'];

			$this->load->view('templates/header');
			$this->load->view('classes/edit', $data);
			$this->load->view('templates/footer');
		}
	}

	public function update($id){
		if($this->session->userdata('role') != 'admin'){
			redirect('home');
		}
		
		$this->class_model->update_class();

		if ($this->class_model->update_class() == false ) {
			// Set message
			$this->session->set_flashdata('class_exists', 'Class already exists');
			redirect('classes/edit/'.$id);
		}
		else {
			// Set message
			$this->session->set_flashdata('class_updated', 'Class has been updated');
			redirect('classes');
		}
	}
}