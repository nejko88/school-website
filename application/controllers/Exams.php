<?php
class Exams extends CI_controller{
	public function add_c(){
    if($this->session->userdata('role') != 'teacher'){
      redirect('home');
    }
    $data = $this->session->all_userdata();
    $data['title'] = 'Add Exam';

    $data['subjects'] = $this->subject_model->get_subjects();
    $data['classes'] = $this->class_model->get_classes();

    $this->form_validation->set_rules('date', 'Date', 'required');

    if ($this->form_validation->run() === FALSE){
      $this->load->view('templates/header', $data);
      $this->load->view('exams/add_c', $data);
      $this->load->view('templates/footer');
    }
    else if ($this->exam_model->add_c() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('exams/add_c', $data);
			$this->load->view('templates/footer');
			// Set message
			$this->session->set_flashdata('exam_exists', 'Exam already exists');
			redirect('exams/add_c');
		}
		else {
			$this->session->set_flashdata('exam_added', 'Exam has been added');
			redirect('profile');
		}
  }

  public function add_m(){
    if($this->session->userdata('role') != 'student'){
      redirect('home');
    }
    $data = $this->session->all_userdata();
    $data['title'] = 'Add Exam';

    $data['subjects'] = $this->subject_model->get_subjects();

    $this->form_validation->set_rules('date', 'Date', 'required');

    if ($this->form_validation->run() === FALSE){
      $this->load->view('templates/header', $data);
      $this->load->view('exams/add_m', $data);
      $this->load->view('templates/footer');
    }
    else if ($this->exam_model->add_m() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('exams/add_m', $data);
			$this->load->view('templates/footer');
			// Set message
			$this->session->set_flashdata('exam_exists', 'Exam already exists');
			redirect('exams/add_m');
		}
		else {
			$this->session->set_flashdata('exam_added', 'Exam has been added');
			redirect('profile');
		}
  }

  public function delete($id) {
		$this->exam_model->delete($id);

		// Set message
		$this->session->set_flashdata('exam_deleted', 'Exam has been deleted');
		redirect('profile');
	}
}