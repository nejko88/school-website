<?php
class Subject_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}
	
	public function get_subjects(){
		$this->db->order_by('teacher', 'ASC');
		$query = $this->db->get('subjects');
		return $query->result_array();
	}

	public function get_subject($id) {
		$query = $this->db->get_where('subjects', array('id' => $id));
		return $query->row_array();
	}

	public function add(){
		$data = array(
			'subject' => $this->input->post('subject'),
			'teacher' => $this->input->post('teacher')
		);
		$query = $this->db->get_where('subjects', $data); 

		if (empty($query->row_array())) {
			$this->db->insert('subjects', $data);
			return true;
		}
		else {
			return false;
		}
	}

	public function delete_subject($id, $subject){
		$subject = str_replace('-', ' ',$subject);
		$subject = explode('_', $subject);

		$array = array('subject' => $subject[1], 'teacher' => $subject[0]);

		$this->db->where('id', $id);
		$this->db->delete('subjects');
		$this->db->where($array); 
		$this->db->delete('schedules');
	}
}