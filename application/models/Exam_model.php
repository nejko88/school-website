<?php
class Exam_model extends CI_Model{
	public function __construct(){
		$this->load->database();
  }

  public function get_teacher_exams($teacher){
    $this->db->order_by('date', 'ASC');
    $query = $this->db->get_where('exams', array('teacher' => $teacher, 'student' => ''));
    return $query->result_array();
  }

  public function get_student_exams($student){
    $this->db->order_by('date', 'ASC');
    $query = $this->db->get_where('exams', array('student' => $student));
    return $query->result_array();
  }

  public function get_class_exams($class){
    $this->db->order_by('date', 'ASC');
    $query = $this->db->get_where('exams', array('class' => $class, 'student' => ''));
    return $query->result_array();
  }

  public function add_C(){
		$data = array(
			'date' => $this->input->post('date'),
      'subject' => $this->input->post('subject'),
      'class' => $this->input->post('class'),
			'teacher' => $this->input->post('teacher'),
		);
		$query = $this->db->get_where('exams', $data); 

		if (empty($query->row_array())) {
			$this->db->insert('exams', $data);
			return true;
		}
		else {
			return false;
		}
  }
  
  public function add_m(){
    $subject = explode(' - ', $this->input->post('subject'))[0];
		$teacher = explode(' - ', $this->input->post('subject'))[1];
		$data = array(
			'date' => $this->input->post('date'),
      'subject' => $subject,
      'class' => $this->input->post('class'),
      'teacher' => $teacher,
      'student' => $this->input->post('student')
		);
		$query = $this->db->get_where('exams', $data); 

		if (empty($query->row_array())) {
			$this->db->insert('exams', $data);
			return true;
		}
		else {
			return false;
		}
  }
  
  public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('exams');
	}
}