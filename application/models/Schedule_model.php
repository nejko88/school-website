<?php
class Schedule_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function get_schedules_classes($id){
			$this->db->where('class_id', $id);
			$this->db->order_by('time', 'ASC');
			$this->db->order_by('day_id', 'ASC');
			$query = $this->db->get('schedules');
			return $query->result_array();
	}

	public function get_schedules_teachers($id){
		$this->db->where('teacher_id', $id);
		$this->db->order_by('time', 'ASC');
		$this->db->order_by('day_id', 'ASC');
		$query = $this->db->get('schedules');
		return $query->result_array();
	}

	public function get_rooms(){
		$query = $this->db->get('rooms');
		return $query->result_array();
	}

	public function get_schedule($id) {
		$query = $this->db->get_where('schedules', array('id' => $id));
		return $query->row_array();
	}

	public function add(){
		$day = explode('-', $this->input->post('day'))[0];
		$day_id = explode('-', $this->input->post('day'))[1];
		$subject = explode(' - ', $this->input->post('subject'))[0];
		$teacher = explode(' - ', $this->input->post('subject'))[1];
		$query = $this->db->get_where('classes', array('class' => $this->input->post('class')));
		$class_id = $query->row_array()['id'];
		$query = $this->db->get_where('users3', array('name' => $teacher));
		$teacher_id = $query->row_array()['id'];

		$data = array(
			'time' => $this->input->post('time'),
			'day' => $day,
			'day_id' => $day_id,
			'subject' => $subject,
			'class' => $this->input->post('class'),
			'teacher' => $teacher,
			'class_id' => $class_id,
			'teacher_id' => $teacher_id,
			'room' => $this->input->post('room')
		);

		$array = array(
			'time' => $this->input->post('time'), 
			'day' => $day,
			'class' => $this->input->post('class')
		);
		$array2 = array(
			'time' => $this->input->post('time'), 
			'day' => $day,
			'teacher' => $teacher
		);
		$array3 = array(
			'time' => $this->input->post('time'), 
			'day' => $day,
			'room' => $this->input->post('room')
		);

		$query = $this->db->get_where('schedules', $array);
		$query2 = $this->db->get_where('schedules', $array2);  
		$query3 = $this->db->get_where('schedules', $array3);  

		if (empty($query->row_array()) && empty($query2->row_array()) && empty($query3->row_array())) {
			$this->db->insert('schedules', $data);
			return true;
		}
		else {
			return false;
		}
	}

	public function delete_schedule($id){
		$this->db->where('id', $id);
		return $this->db->delete('schedules');
	}

	public function update_schedule($id){
		$day = explode('-', $this->input->post('day'))[0];
		$day_id = explode('-', $this->input->post('day'))[1];
		$subject = explode(' - ', $this->input->post('subject'))[0];
		$teacher = explode(' - ', $this->input->post('subject'))[1];
		$query = $this->db->get_where('classes', array('class' => $this->input->post('class')));
		$class_id = $query->row_array()['id'];
		$query = $this->db->get_where('users3', array('name' => $teacher));
		$teacher_id = $query->row_array()['id'];

		$data = array(
			'time' => $this->input->post('time'),
			'day' => $day,
			'day_id' => $day_id,
			'subject' => $subject,
			'class' => $this->input->post('class'),
			'teacher' => $teacher,
			'class_id' => $class_id,
			'teacher_id' => $teacher_id
		);

		$array = array(
			'id !=' => $id,
			'time' => $this->input->post('time'), 
			'day' => $day,
			'class' => $this->input->post('class')
		);
		$array2 = array(
			'id !=' => $id,
			'time' => $this->input->post('time'), 
			'day' => $day,
			'teacher' => $teacher
		);
		$array3 = array(
			'id !=' => $id,
			'time' => $this->input->post('time'), 
			'day' => $day,
			'room' => $this->input->post('room')
		);


		$query = $this->db->get_where('schedules', $array);
		$query2 = $this->db->get_where('schedules', $array2); 
		$query3 = $this->db->get_where('schedules', $array3);

		if (empty($query->row_array()) && empty($query2->row_array()) && empty($query3->row_array())) {
			$this->db->where('id', $id);
			$this->db->update('schedules', $data);
			return true;
		}
		else {
			return false;
		}
	}
}