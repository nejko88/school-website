<?php
class Class_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function get_classes(){
		$this->db->order_by('class', 'ASC');
		$query = $this->db->get('classes');
		return $query->result_array();
	}

	public function get_class($id) {
		$query = $this->db->get_where('classes', array('id' => $id));
		return $query->row_array();
	}

	public function get_users($id) {
		$query = $this->db->get_where('classes', array('id' => $id));
		$class = $query->row_array()['class'];
		$query = $this->db->get_where('users3', array('class' => $class));
		return $query->result_array();
	}

	public function add(){
		$class = $this->input->post('year').' '.$this->input->post('class');

		$data = array(
			'class' => $class
		);

		$query = $this->db->get_where('classes', $data);  

		if (empty($query->row_array())) {
			$this->db->insert('classes', $data);
			return true;
		}
		else {
			return false;
		}
	}

	public function delete_class($id, $class){
		$class = urldecode ($class);
		$this->db->where('id', $id);
		$this->db->delete('classes');
		$this->db->where('class', $class);
		$this->db->delete('schedules');
		$this->db->where('class', $class);
		$this->db->delete('users3');
	}

	public function update_class(){
		$class = $this->input->post('year').' '.$this->input->post('class');
		$query = $this->db->get_where('classes', array('id' => $this->input->post('id')));
		$class2 = $query->row_array()['class'];

		$data = array(
			'class' => $class
		);

		$data2 = array(
			'id !=' => $this->input->post('id'),
			'class' => $class
		);

		$query = $this->db->get_where('classes', $data2);  

		if (empty($query->row_array())) {
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('classes', $data);
			$this->db->where('class', $class2);
			$this->db->update('schedules', $data);
			$this->db->where('class', $class2);
			$this->db->update('users3', $data);
			return true;
		}
		else {
			return false;
		}
	}
}