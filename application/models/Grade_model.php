<?php
class Grade_model extends CI_Model{
	public function __construct(){
		$this->load->database();
		parent::__construct();
  }
  
  public function showSomeStudents(){
		$query = $this->db->get_where('users3', array('class' => $this->input->post('class')));
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
  }

  public function get_grades(){
    $query = $this->db->get('grades');
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
  }

  public function add_grade(){
		$data = array(
			'student' => $this->input->post('student'),
			'subject' => $this->input->post('subject'),
			'semester' => $this->input->post('semester'),
			'grade' => $this->input->post('grade')
		);
		$this->db->insert('grades', $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	public function get_grade($subject, $student) {
		$subject = urldecode ($subject);
		$student = urldecode ($student);
		$this->db->order_by('semester', 'ASC');
		$query = $this->db->get_where('grades', array('subject' => $subject, 'student' => $student));
		return $query->result_array();
	}

	public function showAllGrades($subject, $student){
		$subject = urldecode ($subject);
		$student = urldecode ($student);
		$this->db->order_by('semester', 'ASC');
		$query = $this->db->get_where('grades', array('subject' => $subject, 'student' => $student));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function edit($subject, $student){
		$subject = urldecode ($subject);
		$student = urldecode ($student);
		$id = $this->input->get('id');
		$this->db->where('id', $id);
		$query = $this->db->get('grades');
		if ($query->num_rows() > 0){
			return $query->row();
		}
		else {
			return false;
		}
	}

	public function update(){
		$id = $this->input->post('id');
		$data = array(
			'student' => $this->input->post('student'),
			'subject' => $this->input->post('subject'),
			'semester' => $this->input->post('semester'),
			'grade' => $this->input->post('grade')
		);
		$this->db->where('id', $id);
		$this->db->update('grades', $data);
		return true;
	}

	function delete(){
		$id = $this->input->get('id');
		$this->db->where('id', $id);
		$this->db->delete('grades');
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else{
			return false;
		}
	}
}