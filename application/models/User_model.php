<?php
class User_model extends CI_Model{
	public function __construct(){
		$this->load->database();
		parent::__construct();
	}

	public function search($keyword) {
		$this->db->order_by('name', 'ASC');
		$this->db->like('name', $keyword);
		$query = $this->db->get_where('users3', array('role' => 'student'));
		$this->db->like('name', $keyword);
		$query2 = $this->db->get_where('users3', array('role' => 'teacher'));
		$students = $query->result_array();
		$teachers = $query2->result_array();

		return array(
			'students' => $students,
			'teachers' => $teachers
		);
	}
		
	public function get_users($role = 'all') {
		if ($role === 'all') {
			$this->db->order_by('name', 'ASC');
			$query = $this->db->get('users3');
			return $query->result_array();
		}
		else {
			$this->db->where('role', $role);
			$this->db->order_by('name', 'ASC');
			$query = $this->db->get('users3');
			return $query->result_array();
		}
	}

	public function get_user($id) {
			$query = $this->db->get_where('users3', array('id' => $id));
			return $query->row_array();
	}

	public function add($enc_password){
		$array = explode(' ', $this->input->post('name'));
		for ($i=0; $i < count($array); $i++) { 
			$array[$i] = ucfirst($array[$i]);
		}
		$array = implode(' ', $array);

		//User Data Array
		$data = array(
			'name' => $array,
			'email' => $this->input->post('email'),
			'role' => $this->input->post('role'),
			'password' => $enc_password,
			'class' => $this->input->post('class'),
			'phone' => $this->input->post('phone'),
			'address' => ucfirst($this->input->post('address'))
		);

		//Insert user
		return $this->db->insert('users3', $data);
	}

	public function delete_user($id){
		$this->db->where('id', $id);
		return $this->db->delete('users3');
	}

	public function update_user(){
		$array = explode(' ', $this->input->post('name'));
		for ($i=0; $i < count($array); $i++) { 
			$array[$i] = ucfirst($array[$i]);
		}
		$array = implode(' ', $array);
		$data = array(
			'name' => $array,
			'email' => $this->input->post('email'),
			'role' => $this->input->post('role'),
			'class' => $this->input->post('class'),
			'phone' => $this->input->post('phone'),
			'address' => ucfirst($this->input->post('address'))
		);

		$this->db->where('id', $this->input->post('id'));
		return $this->db->update('users3', $data);
	}
	
	public function check_email_exists($email){
		$query = $this->db->get_where('users3', array('email' => $email));
		if (empty($query->row_array())) {
			return true;
		}
		else {
			return false;
		}
	}
    
  // Log user in
	public function login($name, $password){
		//Validate
		$this->db->where('name', $name);
		$this->db->where('password', $password);

		$result = $this->db->get('users3');

		if ($result->num_rows() == 1) {
			return $result->row_array();
		}

		else {
			return false;
		}
	 }

	 public function get_teachers(){
		$this->db->order_by('name', 'ASC');
		$this->db->where('role', 'teacher');
		$query = $this->db->get('users3');
		$all = $query->result_array();
		$teachers = array();
		for ($i=0; $i < count($all); $i++) {
			array_push($teachers, $all[$i]['name']);
		}
		return $teachers;
	 }

	 public function get_grades($student){
		 $query = $this->db->get_where('grades', array('student' => $student));
		 return $query->result_array();
	 }
}