-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2018 at 12:40 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `class` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `class`) VALUES
(1, '4. c'),
(3, '4. a'),
(6, '4. b'),
(7, '1. a');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `class` varchar(255) NOT NULL,
  `student` varchar(255) NOT NULL,
  `teacher` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `subject`, `date`, `class`, `student`, `teacher`) VALUES
(7, 'biology', '2018-04-21', '4. b', '', 'Daniel Bezjak'),
(9, 'psychology', '2018-04-12', '1. a', 'Nejko Borovinšek', 'Amadej Bezjak'),
(10, 'psychology', '2018-04-26', '1. a', 'Nejko Borovinšek', 'Amadej Bezjak'),
(11, 'english', '2018-04-26', '4. a', 'Marko Skok', 'Simona Skrbiš'),
(12, 'mathematics', '2018-04-13', '4. a', 'Marko Skok', 'Daniel Bezjak'),
(13, 'mathematics', '2018-04-18', '4. a', '', 'Daniel Bezjak'),
(14, 'biology', '2018-04-24', '1. a', '', 'Daniel Bezjak');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `grade` int(11) NOT NULL,
  `student` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `semester`, `subject`, `grade`, `student`) VALUES
(63, 1, 'english', 3, 'Jana Brundula'),
(64, 1, 'biology', 4, 'Marko Skok'),
(65, 1, 'english', 5, 'Jana Brundula'),
(67, 1, 'biology', 3, 'Jana Brundula'),
(68, 1, 'biology', 3, 'Jana Brundula'),
(70, 1, 'english', 3, 'Nejko Borovinšek'),
(74, 1, 'mathematics', 5, 'Nejko Borovinšek'),
(75, 1, 'biology', 4, 'Katarina Kušar'),
(76, 1, 'biology', 3, 'Marko Skok'),
(81, 1, 'mathematics', 2, 'Nejko Borovinšek'),
(82, 3, 'mathematics', 2, 'Jana Brundula'),
(83, 1, 'mathematics', 5, 'Jana Brundula'),
(84, 2, 'mathematics', 3, 'Nejko Borovinšek'),
(85, 2, 'mathematics', 1, 'Jana Brundula'),
(86, 3, 'mathematics', 5, 'Nejko Borovinšek'),
(87, 1, 'mathematics', 4, 'Katarina Kušar'),
(88, 1, 'mathematics', 4, 'Marko Skok'),
(89, 3, 'mathematics', 3, 'Nejko Borovinšek'),
(90, 1, 'mathematics', 3, 'Jana Brundula'),
(91, 1, 'mathematics', 4, 'Jana Brundula');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room`) VALUES
(1, '1.01'),
(2, '1.02'),
(3, '1.03'),
(4, '1.04'),
(5, '1.05'),
(6, '2.01'),
(7, '2.02'),
(8, '2.03'),
(9, '2.04'),
(10, '2.05'),
(11, '3.01'),
(12, '3.02'),
(13, '3.03'),
(14, '3.04'),
(15, '3.05'),
(16, '4.01'),
(17, '4.02'),
(18, '4.03'),
(19, '4.04'),
(20, '4.05');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `class` varchar(255) NOT NULL,
  `day` varchar(255) NOT NULL,
  `day_id` int(11) NOT NULL,
  `teacher` varchar(255) NOT NULL,
  `room` varchar(255) NOT NULL,
  `class_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `subject`, `time`, `class`, `day`, `day_id`, `teacher`, `room`, `class_id`, `teacher_id`) VALUES
(35, 'biology', 7, '4. a', 'monday', 1, 'Daniel Bezjak', '1.01', 3, 15),
(36, 'mathematics', 7, '1. a', 'thursday', 4, 'Daniel Bezjak', '1.02', 7, 15),
(37, 'biology', 7, '1. a', 'tuesday', 2, 'Daniel Bezjak', '1.03', 7, 15),
(38, 'english', 8, '4. a', 'monday', 1, 'Simona Skrbiš', '1.04', 3, 41),
(42, 'biology', 11, '1. a', 'thursday', 4, 'Daniel Bezjak', '2.03', 7, 15),
(43, 'english', 8, '4. c', 'wednesday', 3, 'Simona Skrbiš', '4.03', 1, 41),
(46, 'english', 10, '1. a', 'wednesday', 3, 'Simona Skrbiš', '1.01', 7, 41),
(47, 'geography', 12, '1. a', 'monday', 1, 'Toni Klis', '4.01', 7, 18),
(48, 'geography', 9, '1. a', 'wednesday', 3, 'Toni Klis', '4.04', 7, 18),
(49, 'geography', 8, '1. a', 'thursday', 4, 'Toni Klis', '3.05', 7, 18),
(50, 'geography', 7, '4. b', 'friday', 5, 'Toni Klis', '4.03', 6, 18),
(51, 'geography', 14, '4. a', 'tuesday', 2, 'Toni Klis', '2.01', 3, 18),
(52, 'geography', 11, '4. c', 'wednesday', 3, 'Toni Klis', '1.01', 1, 18);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `teacher` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject`, `teacher`) VALUES
(26, 'biology', 'Daniel Bezjak'),
(27, 'english', 'Simona Skrbiš'),
(28, 'mathematics', 'Daniel Bezjak'),
(29, 'geography', 'Toni Klis');

-- --------------------------------------------------------

--
-- Table structure for table `users3`
--

CREATE TABLE `users3` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users3`
--

INSERT INTO `users3` (`id`, `name`, `address`, `email`, `password`, `class`, `role`, `phone`) VALUES
(1, 'admin', '', 'borovinsek@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '', 'admin', ''),
(15, 'Daniel Bezjak', 'Ferkova 10', 'daniel@gmail.com', '8d788385431273d11e8b43bb78f3aa41', '', 'teacher', '040 333 444'),
(17, 'Marko Skok', '', 'marko@gmail.com', '698d51a19d8a121ce581499d7b701668', '4. a', 'student', ''),
(18, 'Toni Klis', '', 'toni@gmail.com', '698d51a19d8a121ce581499d7b701668', '', 'teacher', ''),
(41, 'Simona Skrbiš', '', 'simona@gmail.com', '698d51a19d8a121ce581499d7b701668', '', 'teacher', '040 444 555'),
(42, 'Jana Brundula', 'Dunajska 5', 'jana@gmail.com', '698d51a19d8a121ce581499d7b701668', '1. a', 'student', ''),
(43, 'Katarina Kušar', 'Smetanova 55', 'kata@gmail.com', 'bcbe3365e6ac95ea2c0343a2395834dd', '4. a', 'student', ''),
(45, 'Nejko Borovinšek', 'Gosposvetska cesta 20', 'b@gmail.com', 'bcbe3365e6ac95ea2c0343a2395834dd', '1. a', 'student', ''),
(46, 'Matej Mulej', 'Leskovec 124', 'mmulej@gmail.com', 'cd73502828457d15655bbd7a63fb0bc8', '4. b', 'student', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users3`
--
ALTER TABLE `users3`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `users3`
--
ALTER TABLE `users3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
